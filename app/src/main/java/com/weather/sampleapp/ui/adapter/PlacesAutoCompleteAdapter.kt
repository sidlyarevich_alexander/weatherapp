package com.weather.sampleapp.ui.adapter

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import com.weather.sampleapp.utils.PlacesUtil

class PlacesAutoCompleteAdapter(context: Context) :
    ArrayAdapter<String>(context, android.R.layout.simple_list_item_1), Filterable {

    private var mStrings = mutableListOf<String>()

    override fun getCount(): Int {
        return mStrings.size
    }

    override fun getItem(position: Int): String? {
        return mStrings[position]
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResult = FilterResults()
                if (constraint != null) {
                    mStrings = PlacesUtil.autoComplete(constraint.toString())
                    filterResult.values = mStrings
                    filterResult.count = mStrings.size
                }
                return filterResult
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (mStrings.isNotEmpty()) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }

        }
    }
}