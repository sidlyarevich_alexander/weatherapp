package com.weather.sampleapp.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.gms.location.*
import com.weather.sampleapp.databinding.ActivityWeatherBinding
import com.weather.sampleapp.db.entity.UserLocationEntity
import com.weather.sampleapp.ui.adapter.PlacesAutoCompleteAdapter
import com.weather.sampleapp.ui.adapter.WeatherAdapter
import com.weather.sampleapp.utils.ConstUtil
import com.weather.sampleapp.utils.NetworkUtil
import com.weather.sampleapp.viewmodel.WeatherViewModel
import dagger.android.support.DaggerAppCompatActivity
import java.security.AccessController.getContext
import java.util.*
import javax.inject.Inject
import kotlin.math.round

class WeatherActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var weatherViewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var networkUtil: NetworkUtil

    private lateinit var viewModel: WeatherViewModel

    private lateinit var binding: ActivityWeatherBinding

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        networkUtil.setConnectionListener()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        viewModel =
            ViewModelProvider(this, weatherViewModelFactory).get(WeatherViewModel::class.java)
        checkPermissions()

        binding = ActivityWeatherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setAutocompleteAdapter()

        observeResults()
    }

    private fun observeResults() {
        observeCurrentWeatherResult()
        observeWeatherFFDResult()
        observeWeatherError()
        observeCurrentWeatherLoader()
        observeWeatherFFDLoader()
    }

    private fun observeCurrentWeatherResult() = viewModel.currentWeatherResult().observe(this,
        {
            if (it != null) {
                val url =
                    "${ConstUtil.OPENWEATHERMAP_ICON}${it.weather[0].icon}${ConstUtil.OPENWEATHERMAP_END}"

                Glide.with(this)
                    .load(url)
                    .into(binding.stateImageView)

                binding.textViewCityName.text = it.name

                val temperature =
                    "${round(it.main.temp_max - (it.main.temp_max - it.main.temp_min) / 2 - 273.15F).toInt()}\u00B0"
                binding.avgTempTextView.text = temperature

                binding.descriptionTextView.text = it.weather[0].main
            }
        })

    private fun observeWeatherFFDResult() = viewModel.weatherDDFResult().observe(this, {
        if (it != null) {
            binding.recyclerViewWeatherWeek.adapter = WeatherAdapter(it.list)
        }
    })

    private fun observeWeatherError() = viewModel.weatherError().observe(this, {
        if (it != null) {
            showToast("Something goes wrong: $it")
        }
    })

    private fun observeCurrentWeatherLoader() = viewModel.currentWeatherLoader().observe(this, {
        showProgressCurrentWeather(it)
    })

    private fun observeWeatherFFDLoader() = viewModel.weatherFFDLoader().observe(this, {
        showProgressWeatherFFD(it)
    })

    private fun setAutocompleteAdapter() {
        val adapter = PlacesAutoCompleteAdapter(this@WeatherActivity)

        binding.textViewAutoComplete.setAdapter(adapter)
        binding.textViewAutoComplete.threshold = 2
        binding.textViewAutoComplete.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->

                val cityName = adapter.getItem(position)?.split(",")?.get(0)
                if (cityName != null) {
                    getData(UserLocationEntity(Date().time, cityName))
                }

                val imm: InputMethodManager =
                    this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(binding.textViewAutoComplete.windowToken, 0)
            }
    }

    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this@WeatherActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@WeatherActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
            )
        } else {
            getLastUserLocation()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if ((ContextCompat.checkSelfPermission(
                            this@WeatherActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) ==
                                PackageManager.PERMISSION_GRANTED)
                    ) {
                        showToast("Permission Granted")

                        getLastUserLocation()
                    }
                } else {
                    showToast("Permission Denied")
                }
                return
            }
        }
    }

    override fun onDestroy() {
        viewModel.disposeElements()
        super.onDestroy()
    }

    @SuppressLint("MissingPermission")
    fun getLastUserLocation() {
        val locationRequest = LocationRequest.create()?.apply {
            interval = 1000
            fastestInterval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                p0 ?: return
                for (location in p0.locations) {
                    val geocoder = Geocoder(applicationContext, Locale.getDefault())
                    val cityName =
                        geocoder.getFromLocation(location.latitude, location.longitude, 1)

                    if (cityName.isNotEmpty()) {
                        val userLocation = UserLocationEntity(
                            Date().time,
                            cityName[0].locality
                        )
                        fusedLocationClient.removeLocationUpdates(this)
                        getData(userLocation)
                    }
                }
            }
        }

        if (locationRequest != null)
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
    }

    private fun getData(userLocationEntity: UserLocationEntity) {
        showProgressCurrentWeather(true)
        showProgressWeatherFFD(true)
        viewModel.pushUserLocation(userLocationEntity)
    }

    private fun showProgressWeatherFFD(show: Boolean) {
        binding.progressBarRecycler.visibility = if (show) View.VISIBLE else View.GONE
        binding.recyclerViewWeatherWeek.visibility = if (show) View.GONE else View.VISIBLE
    }

    private fun showProgressCurrentWeather(show: Boolean) {
        binding.progressBarCurrentWeather.visibility = if (show) View.VISIBLE else View.GONE
        binding.constraintContainer.visibility = if (show) View.GONE else View.VISIBLE
    }

    private fun showToast(message: String) =
        Toast.makeText(this@WeatherActivity, message, LENGTH_LONG)

}