package com.weather.sampleapp.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProfileEntity(
    val name: String = "Empty Name",
    val email: String = "Empty Email",
    val imageUrl: String
) {
    @PrimaryKey(autoGenerate = true)
    var uId: Int? = null

}