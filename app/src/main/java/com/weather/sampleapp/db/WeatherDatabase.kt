package com.weather.sampleapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.weather.sampleapp.db.converter.ListBasConverter
import com.weather.sampleapp.db.converter.WeatherConverter
import com.weather.sampleapp.db.dao.CurrentWeatherDao
import com.weather.sampleapp.db.dao.ProfileDao
import com.weather.sampleapp.db.dao.UserLocationDao
import com.weather.sampleapp.db.dao.WeatherForFiveDaysDao
import com.weather.sampleapp.db.entity.ProfileEntity
import com.weather.sampleapp.db.entity.UserLocationEntity
import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity

@TypeConverters(ListBasConverter::class, WeatherConverter::class)
@Database(
    entities = [UserLocationEntity::class, WeatherForFiveDaysEntity::class, CurrentWeatherEntity::class, ProfileEntity::class],
    version = 1,
    exportSchema = false
)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun getUserLocationDao(): UserLocationDao
    abstract fun getCurrentWeatherDao(): CurrentWeatherDao
    abstract fun getWeatherForFiveDaysDao(): WeatherForFiveDaysDao
    abstract fun getProfileDao(): ProfileDao

}