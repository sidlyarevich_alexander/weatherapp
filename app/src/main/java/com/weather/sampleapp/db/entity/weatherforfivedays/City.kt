package com.weather.sampleapp.db.entity.weatherforfivedays

import androidx.room.Embedded
import com.google.gson.annotations.SerializedName
import com.weather.sampleapp.db.entity.currentweather.Coord

data class City(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @Embedded
    @SerializedName("coord") val coord: Coord,
    @SerializedName("country") val country: String
)