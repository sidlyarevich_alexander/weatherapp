package com.weather.sampleapp.db.entity.weatherforfivedays

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class WeatherForFiveDaysEntity(
    @SerializedName("cod") val cod: Int,
    @SerializedName("message") val message: Double,
    @SerializedName("cnt") val cnt: Int,
    @SerializedName("list") val list: List<ListBas>,
    @Embedded
    @SerializedName("city") val city: City
) {
    @PrimaryKey(autoGenerate = true)
    var uId: Int? = null
}