package com.weather.sampleapp.db.dao

import androidx.room.*
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import io.reactivex.Observable

@Dao
interface WeatherForFiveDaysDao {

    @Query("SELECT * FROM WeatherForFiveDaysEntity")
    fun getWeatherFFD(): Observable<WeatherForFiveDaysEntity>

    @Insert
    fun insert(weatherForFiveDaysEntity: WeatherForFiveDaysEntity)

    @Update
    fun update(weatherForFiveDaysEntity: WeatherForFiveDaysEntity)

    @Delete
    fun delete(weatherForFiveDaysEntity: WeatherForFiveDaysEntity)

    @Query("DELETE FROM WeatherForFiveDaysEntity")
    fun clearDatabase()
}