package com.weather.sampleapp.db.dao

import androidx.room.*
import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import io.reactivex.Maybe
import io.reactivex.Observable

@Dao
interface CurrentWeatherDao {

    @Query("SELECT * FROM CurrentWeatherEntity")
    fun getCurrentWeather(): Observable<CurrentWeatherEntity>

    @Query("SELECT * FROM CurrentWeatherEntity WHERE uId=:id")
    fun getCurrentWeatherById(id: Int): Maybe<CurrentWeatherEntity>

    @Insert
    fun insert(currentWeatherEntity: CurrentWeatherEntity)

    @Update
    fun update(currentWeatherEntity: CurrentWeatherEntity)

    @Delete
    fun delete(currentWeatherEntity: CurrentWeatherEntity)

    @Query("DELETE FROM CurrentWeatherEntity")
    fun clearDatabase()
}