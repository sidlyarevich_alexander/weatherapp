package com.weather.sampleapp.db.entity.weatherforfivedays

import com.google.gson.annotations.SerializedName
import com.weather.sampleapp.db.entity.currentweather.Clouds
import com.weather.sampleapp.db.entity.currentweather.Weather
import com.weather.sampleapp.db.entity.generalweather.Main
import com.weather.sampleapp.db.entity.generalweather.Sys
import com.weather.sampleapp.db.entity.generalweather.Wind

data class ListBas(

    @SerializedName("dt") val dt: Int,
    @SerializedName("main") val main: Main,
    @SerializedName("weather") val weather: List<Weather>,
    @SerializedName("clouds") val clouds: Clouds,
    @SerializedName("wind") val wind: Wind,
    @SerializedName("sys") val sys: Sys,
    @SerializedName("dt_txt") val dt_txt: String
)