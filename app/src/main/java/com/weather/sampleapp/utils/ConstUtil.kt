package com.weather.sampleapp.utils

class ConstUtil {
    companion object {
        const val REQUEST_CODE_SIGN_IN = 1001
        const val OPENWEATHERMAP_ICON = "https://openweathermap.org/img/wn/"
        const val OPENWEATHERMAP_END = "@4x.png"

        const val SERVER_BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val DATABASE_NAME = "weather.db"

        const val API_KEY = "3d7aa461595f45127eabaee80b7b6716"
    }
}