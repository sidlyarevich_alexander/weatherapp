package com.weather.sampleapp.api

import com.weather.sampleapp.db.entity.currentweather.ErrorResponse

interface IRequestCallback<T> {

    fun onSuccess(response: T)
    fun onError(response: ErrorResponse, t: Throwable?)
}
