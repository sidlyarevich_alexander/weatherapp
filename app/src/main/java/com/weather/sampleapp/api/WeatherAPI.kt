package com.weather.sampleapp.api

import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {
    @GET("weather?")
    fun getCurrentWeather(@Query("q") cityName: String, @Query("appid") apiKey: String): Observable<CurrentWeatherEntity>

    @GET("forecast?")
    fun getFFDWeather(@Query("q") cityName: String, @Query("appid") apiKey: String): Observable<WeatherForFiveDaysEntity>
}