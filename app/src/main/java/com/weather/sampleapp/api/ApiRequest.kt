package com.weather.sampleapp.api

import android.util.Log
import com.weather.sampleapp.db.entity.currentweather.ErrorResponse
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class ApiRequest<T> constructor(private val call: Call<T>) {

    fun send(callback: IRequestCallback<T>) {

        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    val body = response.body()
                    if (body != null) {
                        callback.onSuccess(body)
                    }
                } else {
                    var errorResponse = getErrorResponse(response.errorBody())
                    if (errorResponse != null) {
                        callback.onError(errorResponse, null)
                    } else {

                        errorResponse = ErrorResponse()
                        errorResponse.errorCode = response.code()
                        errorResponse.error = "Error code: " + response.code()
                        callback.onError(errorResponse, null)
                    }
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                t.printStackTrace()
                val errorMessage = t::class.java.name + ": " + t.cause?.localizedMessage
                Log.e(ApiRequest::class.java.name, errorMessage)

                val errorResponse = ErrorResponse()
                errorResponse.error = errorMessage
                callback.onError(errorResponse, t)
            }
        })
    }

    private fun getErrorResponse(responseBody: ResponseBody?): ErrorResponse? {
        try {
            if (responseBody != null) {
                return Gson().fromJson(
                    responseBody.string(),
                    ErrorResponse::class.java
                ) as ErrorResponse
            }
        } catch (e: IOException) {
            e.message?.let { Log.e(ApiClient::class.java.name, it) }
        }
        return null
    }
}