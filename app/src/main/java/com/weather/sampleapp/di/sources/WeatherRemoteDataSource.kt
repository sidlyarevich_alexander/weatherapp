package com.weather.sampleapp.di.sources


import com.weather.sampleapp.api.WeatherAPI
import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import com.weather.sampleapp.di.scopes.ApplicationScope
import com.weather.sampleapp.schedulers.BaseSchedulerProvider
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Concrete implementation of the Local Data Source
 */
@ApplicationScope
class WeatherRemoteDataSource @Inject constructor(
    val api: WeatherAPI?,
    val schedulerProvider: BaseSchedulerProvider?
) : IWeatherRemoteDataSource {

    override fun getCurrentWeather(
        city: String,
        apiKey: String
    ): Observable<CurrentWeatherEntity>? = api?.getCurrentWeather(city, apiKey)


    override fun getWeatherFFD(
        city: String,
        apiKey: String
    ): Observable<WeatherForFiveDaysEntity>? = api?.getFFDWeather(city, apiKey)
}
