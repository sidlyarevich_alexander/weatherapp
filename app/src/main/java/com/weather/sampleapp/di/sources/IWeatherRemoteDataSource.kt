package com.weather.sampleapp.di.sources

import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import io.reactivex.Observable

interface IWeatherRemoteDataSource {
    fun getCurrentWeather(city: String, apiKey: String): Observable<CurrentWeatherEntity>?

    fun getWeatherFFD(city: String, apiKey: String): Observable<WeatherForFiveDaysEntity>?
}