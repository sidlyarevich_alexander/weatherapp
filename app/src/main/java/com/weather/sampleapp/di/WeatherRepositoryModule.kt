package com.weather.sampleapp.di

import com.weather.sampleapp.api.WeatherAPI
import com.weather.sampleapp.db.dao.CurrentWeatherDao
import com.weather.sampleapp.db.dao.ProfileDao
import com.weather.sampleapp.db.dao.UserLocationDao
import com.weather.sampleapp.db.dao.WeatherForFiveDaysDao
import com.weather.sampleapp.di.scopes.ApplicationScope
import com.weather.sampleapp.di.scopes.Local
import com.weather.sampleapp.di.scopes.Remote
import com.weather.sampleapp.di.sources.*
import com.weather.sampleapp.schedulers.BaseSchedulerProvider
import dagger.Module
import dagger.Provides

@Module(includes = [WeatherLocalDataModule::class, WeatherRemoteDataModule::class])
class WeatherRepositoryModule {
    @Provides
    @Local
    @ApplicationScope
    fun provideWeatherLocalDataSource(
        currentWeatherDao: CurrentWeatherDao?,
        profileDao: ProfileDao?,
        userLocationDao: UserLocationDao?,
        weatherForFiveDaysDao: WeatherForFiveDaysDao?,
        schedulerProvider: BaseSchedulerProvider?
    ): IWeatherLocalDataSource {
        return WeatherLocalDataSource(
            currentWeatherDao,
            userLocationDao,
            weatherForFiveDaysDao,
            profileDao,
            schedulerProvider
        )
    }

    @Provides
    @Remote
    @ApplicationScope
    fun provideWeatherRemoteDataSource(
        apiService: WeatherAPI?,
        schedulerProvider: BaseSchedulerProvider?
    ): IWeatherRemoteDataSource {
        return WeatherRemoteDataSource(apiService, schedulerProvider)
    }
}