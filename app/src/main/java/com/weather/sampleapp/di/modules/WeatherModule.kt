package com.weather.sampleapp.di.modules

import androidx.lifecycle.ViewModel
import com.weather.sampleapp.di.ViewModelKey
import com.weather.sampleapp.di.scopes.WeatherScope
import com.weather.sampleapp.viewmodel.WeatherViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class WeatherModule {

    @Binds
    @WeatherScope
    @IntoMap
    @ViewModelKey(WeatherViewModel::class)
    abstract fun bindsMainViewModel(weatherViewModel: WeatherViewModel): ViewModel
}