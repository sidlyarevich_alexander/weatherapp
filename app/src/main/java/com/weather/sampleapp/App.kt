package com.weather.sampleapp

import android.app.Application
import com.weather.sampleapp.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication(), HasAndroidInjector {
    companion object {
        lateinit var instance: Application
    }

    private fun initContext() {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        initContext()
    }


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}