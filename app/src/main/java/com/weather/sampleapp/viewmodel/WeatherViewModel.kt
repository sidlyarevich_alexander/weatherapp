package com.weather.sampleapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.weather.sampleapp.db.entity.UserLocationEntity
import com.weather.sampleapp.db.entity.currentweather.CurrentWeatherEntity
import com.weather.sampleapp.db.entity.weatherforfivedays.WeatherForFiveDaysEntity
import com.weather.sampleapp.di.scopes.ApplicationScope
import com.weather.sampleapp.repository.WeatherRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ApplicationScope
class WeatherViewModel @Inject constructor(private val weatherRepository: WeatherRepository) :
    ViewModel() {

    private val currentWeatherResult: MutableLiveData<CurrentWeatherEntity> = MutableLiveData()
    private val weatherFFDResult: MutableLiveData<WeatherForFiveDaysEntity> = MutableLiveData()

    private val weatherError: MutableLiveData<String> = MutableLiveData()

    private val currentWeatherLoader: MutableLiveData<Boolean> = MutableLiveData()
    private val weatherFFDLoader: MutableLiveData<Boolean> = MutableLiveData()

    private lateinit var disposableObserver: DisposableObserver<CurrentWeatherEntity>
    private lateinit var disposableObserverFFD: DisposableObserver<WeatherForFiveDaysEntity>

    private lateinit var usersCity: String

    fun currentWeatherResult(): LiveData<CurrentWeatherEntity> = currentWeatherResult

    fun weatherDDFResult(): LiveData<WeatherForFiveDaysEntity> = weatherFFDResult

    fun weatherError(): LiveData<String> = weatherError

    fun currentWeatherLoader(): LiveData<Boolean> = currentWeatherLoader

    fun weatherFFDLoader(): LiveData<Boolean> = weatherFFDLoader

    fun pushUserLocation(location: UserLocationEntity) {
        usersCity = location.city

        Executors.newSingleThreadExecutor().execute {
            weatherRepository.saveLocation(location)
        }

        loadCurrentWeather()
        loadWeatherFFD()
    }

    private fun loadCurrentWeather() {
        disposableObserver = object : DisposableObserver<CurrentWeatherEntity>() {
            override fun onComplete() {
            }

            override fun onNext(weather: CurrentWeatherEntity) {
                currentWeatherResult.postValue(weather)
                postLoaderValueCurrentWeather()
            }

            override fun onError(e: Throwable) {
                weatherError.postValue(e.message)
                postLoaderValueCurrentWeather()
            }
        }

        weatherRepository.getCurrentWeather(usersCity)
            ?.subscribeOn(Schedulers.io())
            ?.debounce(400, TimeUnit.MILLISECONDS)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(disposableObserver)
    }

    private fun loadWeatherFFD() {
        disposableObserverFFD = object : DisposableObserver<WeatherForFiveDaysEntity>() {
            override fun onComplete() {
            }

            override fun onNext(weather: WeatherForFiveDaysEntity) {
                weatherFFDResult.postValue(weather)
                postLoaderValueWeatherFFD()
            }

            override fun onError(e: Throwable) {
                weatherError.postValue(e.message)
                postLoaderValueWeatherFFD()
            }
        }

        weatherRepository.getWeatherFFD(usersCity)
            ?.subscribeOn(Schedulers.io())
            ?.debounce(400, TimeUnit.MILLISECONDS)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(disposableObserverFFD)
    }

    private fun postLoaderValueCurrentWeather() = currentWeatherLoader.postValue(false)

    private fun postLoaderValueWeatherFFD() = weatherFFDLoader.postValue(false)

    fun disposeElements() {
        if (!disposableObserver.isDisposed) disposableObserver.dispose()
        if (!disposableObserverFFD.isDisposed) disposableObserverFFD.dispose()
    }
}